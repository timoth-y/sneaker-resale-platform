module github.com/timoth-y/sneaker-resale-platform/middleware-service/service-common

go 1.14

require (
	github.com/fatih/structs v1.1.0
	github.com/go-chi/chi v4.1.1+incompatible
	github.com/pkg/errors v0.9.1
	github.com/thoas/go-funk v0.6.0
	go.mongodb.org/mongo-driver v1.3.3
)
