package model

type UserSettings struct {
	Theme string `json:"Theme" bson:"theme"`
}
