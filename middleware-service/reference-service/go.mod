module reference-service

go 1.14

require (
	github.com/Masterminds/squirrel v1.2.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/fatih/structs v1.1.0
	github.com/go-chi/chi v4.1.1+incompatible
	github.com/go-sql-driver/mysql v1.5.0 // indirect
	github.com/golang/protobuf v1.3.1 // indirect
	github.com/google/go-cmp v0.4.0 // indirect
	github.com/jackc/pgx/v4 v4.6.0
	github.com/jfeliu007/goplantuml v1.5.0 // indirect
	github.com/jmoiron/sqlx v1.2.0
	github.com/joho/godotenv v1.3.0
	github.com/pkg/errors v0.9.1
	github.com/rs/xid v1.2.1
	github.com/spf13/afero v1.2.2 // indirect
	github.com/thoas/go-funk v0.6.0
	github.com/timoth-y/sneaker-resale-platform/middleware-service/service-common v0.0.0-20200507015606-23d009a93609
	github.com/vmihailenco/msgpack v3.3.3+incompatible
	go.mongodb.org/mongo-driver v1.3.3
	golang.org/x/net v0.0.0-20200202094626-16171245cfb2 // indirect
	google.golang.org/appengine v1.4.0 // indirect
	gopkg.in/dealancer/validate.v2 v2.1.0
	gopkg.in/yaml.v2 v2.2.2
)
